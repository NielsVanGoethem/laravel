<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        "opName", "landmarkId", "description"
    ];

    public function landmark() {
        return $this->hasOne("App\Landmark");
    }
}
