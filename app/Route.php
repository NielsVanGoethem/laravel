<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
   public function landmarks() {
       return $this->belongsToMany("App\Landmark");
   }

   public function reservations() {
       return $this->hasMany("App\Reservation");
   }

}
