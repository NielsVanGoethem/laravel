<?php

namespace App;

use Minishlink\WebPush\Notification;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

class LandmarkSuggested extends Notification
{
    private $landmarkName;

    /**
     * LandmarkSuggested constructor.
     */
    public function __construct($landmarkName)
    {
        $this->landmarkName = $landmarkName;
    }

    public function via($notifiable) {
        return [WebPushChannel::class];
    }

    public function toWebPush($notifiable, $notification) {
        $message = "There is a new landmark suggested.";

        return (new WebPushMessage)
            ->title("New landmark suggested")
            ->body($message);
    }
}
