<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    public $timestamps = false;

    protected $fillable = ["user_id"];

    public function user() {
        return $this->belongsTo("App\User");
    }

    public function route() {
        return $this->belongsTo("App\Route");
    }

}
