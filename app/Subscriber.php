<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use NotificationChannels\WebPush\HasPushSubscriptions;

class Subscriber extends Model
{
    use HasPushSubscriptions;
}
