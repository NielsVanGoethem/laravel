<?php

namespace App\Http\Controllers;

use App\Landmark;
use App\Post;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function getPosts()
    {
        $posts = Post::orderBy("created_at", "desc")->get();
        $landmarks = Landmark::all();

        return response()->json(["posts" => $posts, "landmarks" => $landmarks]);
    }

    public function getAccountInfo()
    {
        $user = auth('api')->user();
        $badges = collect();

        foreach ($user->badges as $badge) {
            $badges->push($badge->landmark);
        }

        return response()->json(["user" => $user]);
    }

    public function makePost(Request $request)
    {
        $user = auth("api")->user();
        $post = Post::create([
            "opName" => $user->name,
            "landmarkId" => $request->landmark_id,
            "description" => $request->message
        ]);
        $post->save();
        return response()->json(["success" => true]);
    }

    public function getLandmarkImage($imageName)
    {
        $image = $imageName . ".png";
        return response()->download(public_path($image));
    }

    public function updateAccountInfo(Request $request)
    {
        $user = auth("api")->user();
        if ($request->name != null) {
            $user->name = $request->name;
        } else if ($request->email != null) {
            $user->email = $request->email;
        }
        $user->save();
        return response()->json(["success" => $request->all()]);
    }
}
