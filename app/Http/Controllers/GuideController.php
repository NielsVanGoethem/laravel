<?php

namespace App\Http\Controllers;

use App\LandmarkSuggested;
use App\Reservation;
use App\Route;
use App\Subscriber;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class GuideController extends Controller
{
    public function routes()
    {
        $routes = Route::all();

        return response()->json(["routes" => $routes]);
    }

    public function order(Request $request)
    {
        $data = $this->validateForm($request);
        $user = auth('api')->user();
        $user_id = $user->getAuthIdentifier();
        $route = Route::find($data["route_id"]);
        $price = $data["amount_of_people"] * $route->price;

        $reservation = new Reservation;

        $reservation->user_id = $user_id;
        $reservation->route_id = $data["route_id"];
        $reservation->people_amount = $data["amount_of_people"];
        $reservation->date = $data["date"];
        $reservation->price = $price;

        $reservation->save();

        return response()->json(["reservation" => $reservation, "user" => $user, "route" => $route]);
    }

    private function validateForm(Request $request)
    {
        $rules = [
            "route_id" => "required|integer",
            "date" => "required|date|after:today",
            "amount_of_people" => "required|integer"
        ];

        return $request->validate($rules);
    }

    public function suggestLandmark(Request $request) {
        $landmarkName = $request["name"];
        Notification::send(Subscriber::all(), new LandmarkSuggested($landmarkName));
        return Response("OK", 200);
    }
}
