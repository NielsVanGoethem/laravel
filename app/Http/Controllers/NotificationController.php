<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function saveSubscription(Request $request) {
        $data = json_decode($request->getContent(), true);

        $json = $data["notificationJson"];

        $subscriber = new Subscriber();
        $subscriber->notificationJson = json_decode($json);
        $subscriber->save();

        $endpoint = $json["endpoint"];
        $keys = $json["keys"];

        $subscriber->updatePushSubscription($endpoint, $keys["p256dh"], $keys["auth"], null);
    }
}
