<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Landmark extends Model
{
    public $timestamps = false;

    public function routes() {
        return $this->belongsToMany("App\Route");
    }

    public function badge() {
        return $this->hasOne("App\Badge");
    }

}
