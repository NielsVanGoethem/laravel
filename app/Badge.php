<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    public function users() {
        return $this->belongsToMany("App\User");
    }

    public function landmark() {
        return $this->belongsTo("App\Landmark");
    }
}
