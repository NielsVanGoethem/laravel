<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class RouteLandmark extends Pivot
{
    protected $table = "routes_landmarks";
    public $timestamps = false;
}
