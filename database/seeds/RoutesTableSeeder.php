<?php

use App\Route;
use App\RouteLandmark;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class RoutesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode(Storage::get("routes.json"));

        foreach ($json->routes as $route) {
            $routeToInsert = Route::create([
                "name" => $route->route,
                "description" => $route->description,
                "duration" => $route->duration,
                "price" => $route->price,
                "img" => $route->img,
                "external_id" => $route->id
            ]);

            $routeToInsert->save();

            $route_id = $routeToInsert->id;

            foreach ($route->landmarks as $landmark) {
                $routeLandmarkToInsert = RouteLandmark::create([
                    "route_id" => $route_id,
                    "landmark_id" => $landmark
                ]);
                $routeLandmarkToInsert->save();
            }
        }
    }
}
