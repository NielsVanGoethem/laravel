<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([LandmarksTableSeeder::class, PostsTableSeeder::class, RoutesTableSeeder::class, UserTableSeeder::class]);
    }
}
