<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class LandmarksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = Storage::get("landmarks.json");
        $data = json_decode($json);

        foreach ($data->landmarks as $landmark) {
            $landmarkToInsert = \App\Landmark::create([
                "name" => $landmark->landmark,
                "place" => $landmark->place,
                "city" => $landmark->city,
                "text" => $landmark->text,
                "source" => $landmark->source,
                "badge_img" => $landmark->badge_img,
                "coordinates" => json_encode($landmark->coordinates),
                "external_id" => $landmark->id
            ]);

            $landmarkToInsert->save();
        }
    }
}
