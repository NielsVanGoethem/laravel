<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 15)->create();
        $defaultUser = \App\User::create([
            "name" => "Niels",
            "email" => "nielsvangoethem@hotmail.com",
            "password" => "Niels123"
        ]);
        $defaultUser->save();

        $id = $defaultUser->id;

        $reservation = \App\Reservation::create([
            "user_id" => $id,
            "route_id" => 2,
            "people_amount" => 4,
            "date" => "2024-04-02",
            "price" => 60
        ]);

        $reservation->save();

        $reservation2 = \App\Reservation::create([
            "user_id" => $id,
            "route_id" => 1,
            "people_amount" => 4,
            "date" => "2024-04-02",
            "price" => 60
        ]);

        $reservation2->save();

        for ($i = 0; $i < 5; $i++) {
            $badge = \App\Badge::create([
                "user_id" => $id,
                "landmark_id" => $i + 1
            ]);
            $badge->save();
        }

    }
}
