<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Landmark;
use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {

    $landmarks = Landmark::all()->pluck("id")->toArray();

    return [
        "opName" => $faker->firstName,
        "landmarkId" => $faker->randomElement($landmarks),
        "img" => $faker->boolean(25) ? $faker->imageUrl() : null,
        "description" => $faker->boolean(65) ? $faker->sentence : null
    ];
});
