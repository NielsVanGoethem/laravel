<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Landmark;
use Faker\Generator as Faker;

$factory->define(Landmark::class, function (Faker $faker) {
    return [
        "name" => $faker->randomElement([
            "Belfry",
            "Basilica of the Holy Blood",
            "Market",
            "Town Hall",
            "Liberty of Bruges",
            "Church of Our Lady",
            "Old Saint John's Hospital",
            "Béguinage",
            "Jerusalem Church",
            "I Love Coffee",
            "Balthasar",
            "Brewery"
        ]),
        "city" => "Bruges",
        "badge_img" => "images/belfry_tower.png"
    ];
});
