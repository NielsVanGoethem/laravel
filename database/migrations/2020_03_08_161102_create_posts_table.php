<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("opName");
            $table->bigInteger("landmarkId")->unsigned();
            $table->string("img")->nullable();
            $table->string("description")->nullable();
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->foreign("landmarkId")->references("id")->on("landmarks");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
