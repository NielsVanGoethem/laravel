<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("user_id")->unsigned();
            $table->bigInteger("route_id")->unsigned();
            $table->integer("people_amount");
            $table->date("date");
            $table->double("price");
        });

        Schema::table('reservations', function (Blueprint $table) {
            $table->foreign("user_id")->references("id")->on("users");
            $table->foreign("route_id")->references("id")->on("routes");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
