<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoutesLandmarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes_landmarks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("route_id")->unsigned();
            $table->bigInteger("landmark_id")->unsigned();
        });

        Schema::table('routes_landmarks', function (Blueprint $table) {
            $table->foreign('route_id')->references('id')->on('routes');
            $table->foreign('landmark_id')->references('id')->on('landmarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes_landmarks');
    }
}
